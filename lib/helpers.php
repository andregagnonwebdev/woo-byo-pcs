<?php
/**
 * Helper functions
 *
 * @package         Woo_Byo_Pcs
 */

 function BYO_GetSoapResult( $string, &$error) {

   $xml = simplexml_load_string( $string, NULL, NULL, "http://schemas.xmlsoap.org/soap/envelope/");
   $ns = $xml->getNamespaces( true );
   $soap = $xml->children( $ns['soap'] );
   $res = $soap->Body->children();
   // var_dump ( $res );
   // print_r( $res);
   // var_dump( $res->NewOrderValidateResponse);
   $xml = simplexml_load_string( $res->NewOrderValidateResponse->NewOrderValidateResult);
   // var_dump( $xml);
   // var_dump( $xml->Success);
   // echo " sucesss: ".$xml->Success;
   $result = ( $xml->Success == 1) ? true : false;

   //var_dump( $xml );

   if ( !$result) {
     $error = $xml->Errors->Error[ 1]->ErrMessage;
   }
   else {
     $error = '';
   }

   return( $result);
 }


function BYO_SetData( &$headers, &$body, $pcs, $customer_data, $product_data ) {

  $headers = array(
     'Host' => $pcs[ 'Host'],
     'Content-Type' => 'text/xml; charset=utf-8',
     'SOAPAction' => $pcs[ 'SOAPActionNewOrder'],
     );

  $soap_header = '';
  $soap_header .= '<soap:Header>';
  $soap_header .= '<NewOrderAuthenticationHeader xmlns="'.$pcs[ 'xmlns'] .'">';
  $soap_header .= '<UserID>'.$pcs[ 'UserID'].'</UserID>';
  $soap_header .= '<Password>'.$pcs[ 'Password'].'</Password>';
  $soap_header .= '</NewOrderAuthenticationHeader>';
  $soap_header .= '</soap:Header>';

  $soap_body = '';
  $soap_body .= '<soap:Body>';
  $soap_body .= '<NewOrderValidate xmlns="'.$pcs[ 'xmlns'].'">';
  $soap_body .= '<PUB>'.$pcs[ 'PUB'].'</PUB>';
  $soap_body .= '<FNAME>'.$customer_data[ 'shipping_first_name'].'</FNAME>';
  $soap_body .= '<LNAME>'.$customer_data[ 'shipping_last_name'].'</LNAME>';
  $soap_body .= '<PREFIX></PREFIX>';
  $soap_body .= '<SUFFIX></SUFFIX>';
  $soap_body .= '<TITLE></TITLE>';
  $soap_body .= '<DEPT></DEPT>';
  $soap_body .= '<COMPANY></COMPANY>';
  $soap_body .= '<STREET1>'.$customer_data[ 'shipping_address_1'].'</STREET1>';
  $soap_body .= '<STREET2>'.$customer_data[ 'shipping_address_2'].'</STREET2>';
  $soap_body .= '<CITY>'.$customer_data[ 'shipping_city'].'</CITY>';
  $soap_body .= '<STATE>'.$customer_data[ 'shipping_state'].'</STATE>';
  $soap_body .= '<ZIP>'.$customer_data[ 'shipping_postcode'].'</ZIP>';
  $soap_body .= '<COUNTRYCODE>'.$customer_data[ 'shipping_country'].'</COUNTRYCODE>';
  $soap_body .= '<RENTPOSTAL></RENTPOSTAL>';
  $soap_body .= '<RENTEMAIL></RENTEMAIL>';
  $soap_body .= '<RENTPHONE></RENTPHONE>';
  $soap_body .= '<RENTFAX></RENTFAX>';
  $soap_body .= '<PHONE></PHONE>';
  $soap_body .= '<FAX></FAX>';
  $soap_body .= '<EMAIL></EMAIL>';
  $soap_body .= '<TRACK>'.$pcs[ 'TRACK'].'</TRACK>';
  $soap_body .= '<TERM>8</TERM>';
  $soap_body .= '<COPIES>1</COPIES>';
  $soap_body .= '<PREMIUM></PREMIUM>';
  $soap_body .= '<SERVICE></SERVICE>';
  $soap_body .= '<PRICE>'.$product_data[ 'price'].'</PRICE>';
  $soap_body .= '<TAX>0.00</TAX>';
  $soap_body .= '<PAID>'.$product_data[ 'price'].'</PAID>';
  $soap_body .= '<PAYTYPE>O</PAYTYPE>';
  $soap_body .= '<RENSER></RENSER>';
  $soap_body .= '<CC_TX_ID></CC_TX_ID>';
  $soap_body .= '<CC_TOKEN></CC_TOKEN>';
  $soap_body .= '<CLIENTORDERID></CLIENTORDERID>';
  $soap_body .= '<VALIDATION_ONLY>0</VALIDATION_ONLY>';
  $soap_body .= '<DONEE_FNAME></DONEE_FNAME>';
  $soap_body .= '<DONEE_LNAME></DONEE_LNAME>';
  $soap_body .= '<DONEE_PREFIX></DONEE_PREFIX>';
  $soap_body .= '<DONEE_SUFFIX></DONEE_SUFFIX>';
  $soap_body .= '<DONEE_TITLE></DONEE_TITLE>';
  $soap_body .= '<DONEE_DEPT></DONEE_DEPT>';
  $soap_body .= '<DONEE_COMPANY></DONEE_COMPANY>';
  $soap_body .= '<DONEE_STREET1></DONEE_STREET1>';
  $soap_body .= '<DONEE_STREET2></DONEE_STREET2>';
  $soap_body .= '<DONEE_CITY></DONEE_CITY>';
  $soap_body .= '<DONEE_STATE></DONEE_STATE>';
  $soap_body .= '<DONEE_ZIP></DONEE_ZIP>';
  $soap_body .= '<DONEE_COUNTRYCODE></DONEE_COUNTRYCODE>';
  $soap_body .= '<DONEE_PHONE></DONEE_PHONE>';
  $soap_body .= '<DONEE_FAX></DONEE_FAX>';
  $soap_body .= '<DONEE_EMAIL></DONEE_EMAIL>';
  $soap_body .= '<CREATE_PROFILE>0</CREATE_PROFILE>';
  $soap_body .= '<USERNAME></USERNAME>';
  $soap_body .= '<PASSWORD></PASSWORD>';
  $soap_body .= '<EXT_CUST_ID></EXT_CUST_ID>';
  $soap_body .= '<DONEE_EXT_CUST_ID></DONEE_EXT_CUST_ID>';
  $soap_body .= '</NewOrderValidate>';
  $soap_body .= '</soap:Body>';

  // build body
  $body = '<?xml version="1.0" encoding="utf-8"?>';
  $body .= '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">';
  $body .= $soap_header;
  $body .= $soap_body;
  $body .= '</soap:Envelope>';

  // remove white space from XML
  $body = str_replace( array( '\n', '\r', '\t'), '', $body);

}

$body = '<?xml version="1.0" encoding="utf-8" ?>';

/**
 * Converts the WooCommerce country codes to 3-letter ISO codes
 * Modified for PCS publishing woo-byo-pcs plugin
 * @param string WooCommerce's 2 letter country code
 * @return string ISO 3-letter country code
 */
function BYO_convert_country_code( $country ) {
      $countries = array(
            'AF' => 'AFG', //Afghanistan
            'AL' => 'ALB', //Albania
            'DZ' => 'ALG', //Algeria
            'AD' => 'AND', //Andorra
            'AO' => 'ANGO', //Angola
            'AI' => 'ANGU', //Anguilla
            'AG' => 'ANT', //Antigua and Barbuda
            'AR' => 'ARG', //Argentina
            'AM' => 'ARM', //Armenia
            'AW' => 'ARU', //Aruba
            'AU' => 'AUSL', //Australia
            'AT' => 'AUSR', //Austria
            'AZ' => 'AZE', //Azerbaijan
            'BS' => 'BAHM', //Bahamas
            'BH' => 'BAHR', //Bahrain
            'BD' => 'BAN', //Bangladesh
            'BB' => 'BAR', //Barbados
            'BY' => 'BELA', //Belarus
            'BE' => 'BELG', //Belgium
            'BZ' => 'BELI', //Belize
            'BJ' => 'BEN', //Benin
            'BM' => 'BER', //Bermuda
            'BT' => 'BHU', //Bhutan
            'BO' => 'BOL', //Bolivia
            //'BQ' => 'BES', //Bonaire, Saint Estatius and Saba
            'BA' => 'BOS', //Bosnia and Herzegovina
            'BW' => 'BOT', //Botswana
            //'BV' => 'BVT', //Bouvet Islands
            'BR' => 'BRA', //Brazil
            //'IO' => 'IOT', //British Indian Ocean Territory
            'BN' => 'BRU', //Brunei
            'BG' => 'BUL', //Bulgaria
            'BF' => 'BUR', //Burkina Faso
            'BI' => 'BURU', //Burundi
            'KH' => 'CAMB', //Cambodia
            'CM' => 'CAMR', //Cameroon
            'CA' => 'CAN', //Canada
            'CV' => 'CAP', //Cape Verde
            'KY' => 'CAY', //Cayman Islands
            'CF' => 'CAR', //Central African Republic
            'TD' => 'CHA', //Chad
            'CL' => 'CHIL', //Chile
            'CN' => 'CHIN', //China
            // 'CX' => 'CXR', //Christmas Island
            // 'CC' => 'CCK', //Cocos (Keeling) Islands
            'CO' => 'COL', //Colombia
            'KM' => 'COM', //Comoros
            'CG' => 'CON', //Congo
            'CD' => 'CON', //Congo, Democratic Republic of the
            'CK' => 'COOK', //Cook Islands
            'CR' => 'COS', //Costa Rica
            'CI' => 'COT', //Côte d\'Ivoire
            'HR' => 'CRO', //Croatia
            'CU' => 'CUB', //Cuba
            'CW' => 'CUR', //Curaçao
            'CY' => 'CYP', //Cyprus
            'CZ' => 'CZE', //Czech Republic
            'DK' => 'DEN', //Denmark
            'DJ' => 'DJI', //Djibouti
            // 'DM' => 'DMA', //Dominica
            'DO' => 'DOM', //Dominican Republic
            'EC' => 'ECUD', //Ecuador
            'EG' => 'EGY', //Egypt
            'SV' => 'ELS', //El Salvador
            'GQ' => 'ECUG', //Equatorial Guinea
            'ER' => 'ERIT', //Eritrea
            'EE' => 'EST', //Estonia
            'ET' => 'ETH', //Ethiopia
            'FK' => 'FAL', //Falkland Islands
            'FO' => 'FAE', //Faroe Islands
            'FJ' => 'FIJ', //Fiji
            'FI' => 'FIN', //Finland
            'FR' => 'FRA', //France
            'GF' => 'FGUI', //French Guiana
            'PF' => 'FPOL', //French Polynesia
            // 'TF' => 'ATF', //French Southern Territories
            'GA' => 'GAB', //Gabon
            'GM' => 'GAM', //Gambia
            'GE' => 'GEO', //Georgia
            'DE' => 'GER', //Germany
            'GH' => 'GHA', //Ghana
            'GI' => 'GIB', //Gibraltar
            'GR' => 'GREC', //Greece
            'GL' => 'GREE', //Greenland
            // 'GD' => 'GRD', //Grenada
            'GP' => 'GAUD', //Guadeloupe
            // 'GU' => 'GUM', //Guam
            'GT' => 'GAUT', //Guatemala
            'GG' => 'GUE', //Guernsey
            'GN' => 'GUI', //Guinea
            'GW' => 'GUIB', //Guinea-Bissau
            'GY' => 'GUY', //Guyana
            'HT' => 'HAI', //Haiti
            // 'HM' => 'HMD', //Heard Island and McDonald Islands
            // 'VA' => 'VAT', //Holy See (Vatican City State)
            'HN' => 'HOND', //Honduras
            'HK' => 'HONG', //Hong Kong
            'HU' => 'HUN', //Hungary
            'IS' => 'ICE', //Iceland
            'IN' => 'INDI', //India
            'ID' => 'INDO', //Indonesia
            'IR' => 'IRAN', //Iran
            'IQ' => 'IRAQ', //Iraq
            'IE' => 'IRE', //Republic of Ireland
            // 'IM' => 'IMN', //Isle of Man
            'IL' => 'ISR', //Israel
            'IT' => 'ITA', //Italy
            'JM' => 'JAM', //Jamaica
            'JP' => 'JAP', //Japan
            'JE' => 'JER', //Jersey
            'JO' => 'JOR', //Jordan
            'KZ' => 'KAZ', //Kazakhstan
            'KE' => 'KEN', //Kenya
            'KI' => 'KIR', //Kiribati
            // 'KP' => 'PRK', //Korea, Democratic People\'s Republic of
            'KR' => 'SKOR', //Korea, Republic of (South)
            'KW' => 'KUW', //Kuwait
            'KG' => 'KYR', //Kyrgyzstan
            'LA' => 'LAO', //Laos
            'LV' => 'LAT', //Latvia
            'LB' => 'LEN', //Lebanon
            'LS' => 'LES', //Lesotho
            'LR' => 'LIBR', //Liberia
            'LY' => 'LIBY', //Libya
            'LI' => 'LIC', //Liechtenstein
            'LT' => 'LIT', //Lithuania
            'LU' => 'LUX', //Luxembourg
            'MO' => 'MAC', //Macao S.A.R., China
            'MK' => 'MKD', //Macedonia
            'MG' => 'MAD', //Madagascar
            'MW' => 'MALW', //Malawi
            'MY' => 'MALS', //Malaysia
            'MV' => 'MALD', //Maldives
            'ML' => 'MALI', //Mali
            'MT' => 'MALT', //Malta
            // 'MH' => 'MHL', //Marshall Islands
            'MQ' => 'MAR', //Martinique
            'MR' => 'MRTN', //Mauritania
            'MU' => 'MRTS', //Mauritius
            // 'YT' => 'MYT', //Mayotte
            'MX' => 'MEX', //Mexico
            // 'FM' => 'FSM', //Micronesia
            'MD' => 'MOD', //Moldova
            'MC' => 'MONA', //Monaco
            'MN' => 'MONG', //Mongolia
            'ME' => 'MONT', //Montenegro
            'MS' => 'MONS', //Montserrat
            'MA' => 'MOR', //Morocco
            'MZ' => 'MOZ', //Mozambique
            'MM' => 'MYAN', //Myanmar
            'NA' => 'NAM', //Namibia
            'NR' => 'NAU', //Nauru
            'NP' => 'NEP', //Nepal
            'NL' => 'NETH', //Netherlands
            'AN' => 'NETA', //Netherlands Antilles
            'NC' => 'NEWC', //New Caledonia
            'NZ' => 'NEWZ', //New Zealand
            'NI' => 'NIC', //Nicaragua
            'NE' => 'NIGR', //Niger
            'NG' => 'NIGA', //Nigeria
            // 'NU' => 'NIU', //Niue
            // 'NF' => 'NFK', //Norfolk Island
            // 'MP' => 'MNP', //Northern Mariana Islands
            'NO' => 'NOR', //Norway
            'OM' => 'OMA', //Oman
            'PK' => 'PAK', //Pakistan
            'PW' => 'PAL', //Palau
            // 'PS' => 'PSE', //Palestinian Territory
            'PA' => 'PAN', //Panama
            'PG' => 'PAP', //Papua New Guinea
            'PY' => 'PAR', //Paraguay
            'PE' => 'PER', //Peru
            'PH' => 'PHIL', //Philippines
            // 'PN' => 'PCN', //Pitcairn
            'PL' => 'POL', //Poland
            'PT' => 'POR', //Portugal
            // 'PR' => 'PRI', //Puerto Rico
            'QA' => 'QAT', //Qatar
            'RE' => 'REU', //Reunion
            'RO' => 'ROM', //Romania
            'RU' => 'RUS', //Russia
            'RW' => 'RWA', //Rwanda
            // 'BL' => 'BLM', //Saint Barth&eacute;lemy
            'SH' => 'STH', //Saint Helena
            // 'KN' => 'KNA', //Saint Kitts and Nevis
            'LC' => 'SLC', //Saint Lucia
            'MF' => 'STMR', //Saint Martin (French part)
            'SX' => 'STRM', //Sint Maarten / Saint Matin (Dutch part)
            // 'PM' => 'SPM', //Saint Pierre and Miquelon
            // 'VC' => 'VCT', //Saint Vincent and the Grenadines
            // 'WS' => 'WSM', //Samoa
            'SM' => 'SMAR', //San Marino
            // 'ST' => 'STP', //S&atilde;o Tom&eacute; and Pr&iacute;ncipe
            'SA' => 'SAU', //Saudi Arabia
            'SN' => 'SEN', //Senegal
            'RS' => 'SER', //Serbia
            'SC' => 'SEY', //Seychelles
            'SL' => 'SIE', //Sierra Leone
            'SG' => 'SIN', //Singapore
            'SK' => 'SLOV', //Slovakia
            'SI' => 'SLO', //Slovenia
            'SB' => 'SOL', //Solomon Islands
            'SO' => 'SOM', //Somalia
            'ZA' => 'SAFR', //South Africa
            // 'GS' => 'SGS', //South Georgia/Sandwich Islands
            // 'SS' => 'SSD', //South Sudan
            'ES' => 'SPA', //Spain
            'LK' => 'SRI', //Sri Lanka
            'SD' => 'SUD', //Sudan
            'SR' => 'SUR', //Suriname
            // 'SJ' => 'SJM', //Svalbard and Jan Mayen
            'SZ' => 'SWI', //Swaziland
            'SE' => 'SWE', //Sweden
            'CH' => 'SWI', //Switzerland
            'SY' => 'SYR', //Syria
            'TW' => 'TAI', //Taiwan
            'TJ' => 'TAJ', //Tajikistan
            'TZ' => 'TAN', //Tanzania
            'TH' => 'THA', //Thailand
            // 'TL' => 'TLS', //Timor-Leste
            'TG' => 'TON', //Togo
            // 'TK' => 'TKL', //Tokelau
            'TO' => 'TON', //Tonga
            'TT' => 'TRIN', //Trinidad and Tobago
            'TN' => 'TUN', //Tunisia
            'TR' => 'TRKY', //Turkey
            'TM' => 'TRKM', //Turkmenistan
            // 'TC' => 'TCA', //Turks and Caicos Islands
            'TV' => 'TUV', //Tuvalu
            'UG' => 'UGA', //Uganda
            'UA' => 'UKR', //Ukraine
            'AE' => 'UAE', //United Arab Emirates
            'GB' => 'UK', //United Kingdom
            'US' => 'USA', //United States
            // 'UM' => 'UMI', //United States Minor Outlying Islands
            'UY' => 'URU', //Uruguay
            'UZ' => 'UZB', //Uzbekistan
            'VU' => 'VAN', //Vanuatu
            'VE' => 'VEN', //Venezuela
            'VN' => 'VIET', //Vietnam
            'VG' => 'BVI', //Virgin Islands, British
            // 'VI' => 'VIR', //Virgin Island, U.S.
            // 'WF' => 'WLF', //Wallis and Futuna
            // 'EH' => 'ESH', //Western Sahara
            'YE' => 'YEM', //Yemen
            'ZM' => 'ZAM', //Zambia
            'ZW' => 'ZIM', //Zimbabwe
      );
      $pcs_code = isset( $countries[$country] ) ? $countries[$country] : $country;
      return $pcs_code;
}
