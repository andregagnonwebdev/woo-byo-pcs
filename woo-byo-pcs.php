<?php
/**
 * Plugin Name:     Woo BYO-PCS SOAP Transactions
 * Plugin URI:      http://sterlingwoodsgroup.com
 * Description:     Handle SOAP transcations with PCS for BYO subscriptions in WooCoommerce.
 * Author:          Andre Gagnon
 * Author URI:      https://andregagnon.com
 * Text Domain:     woo-byo-pcs
 * Domain Path:     /languages
 * Version:         0.1.7
 *
 * @package         Woo_Byo_Pcs
 */

require plugin_dir_path( __FILE__) . '/lib/helpers.php';

// on new gift subscription has been activated.
// process action, finder order, start transcation with PCS, return results
add_action('woocommerce_subscription_status_active', 'BYO_SoapNewSubscription', 10, 1);
add_action('woocommerce_subscription_status_updated', 'BYO_SoapNewSubscription2', 10, 1);
add_action('woocommerce_subscription_status_pending', 'BYO_SoapNewSubscription3', 10, 1);
function BYO_SoapNewSubscription( $subscription) {
  if ( !$subscription)
    return;
  $id = $subscription->ID;

  $result = 'test';
  return( $result);

}
function BYO_SoapNewSubscription2( $subscription) {
  if ( !$subscription)
    return;
  return( true);
}
function BYO_SoapNewSubscription3( $subscription) {
  if ( !$subscription)
    return;
  return( true);
}


// on new order completed, process action, start transcation with PCS, return results
//  order completed on woo-commerce side
//add_action('woocommerce_order_status_completed', 'BYO_TestSoap', 10, 1); //DNW?
add_action('woocommerce_payment_complete', 'BYO_SoapNewOrder', 10, 1);


$BYO_pcs_data = array();
$BYO_pcs_data[ 'URL'] = 'https://subscribe.pcspublink.com/WSTransactions_v1_1_9/WSTransactions_v1_1_9.asmx';
$BYO_pcs_data[ 'Host'] = 'subscribe.pcspublink.com';
$BYO_pcs_data[ 'SOAPActionNewOrder'] = '"https://subscribe.pcspublink.com/WSTransactions_v1_1_9/WSTransactions_v1_1_9.asmx/NewOrderValidate"';

// sandbox test credentials
$BYO_pcs_data[ 'UserID'] = 'BREW_SWG#%*';
$BYO_pcs_data[ 'Password'] = '0xF1968C02C753EF19A9606FA5B702C98B';

$BYO_pcs_data[ 'xmlns'] = 'https://subscribe.pcspublink.com/WSTransactions_v1_1_9/WSTransactions_v1_1_9.asmx';
$BYO_pcs_data[ 'PUB'] = 'BREW';
$BYO_pcs_data[ 'TRACK'] = '9SW1F'; // new order


function BYO_SoapNewOrder( $order_id) {
  if ( !$order_id )
      return;

  // get product info
  // is this a print-subscription?
   $print_subscribe = false;

   // Get order object
   $order = wc_get_order( $order_id );

   // Iterating through each order items (WC_Order_Item_Product objects in WC 3+)
   foreach ( $order->get_items() as $item_id => $item_values ) {

       // Product_id
       $product_id = $item_values->get_product_id();

       // OR the Product id from the item data
       $item_data = $item_values->get_data();
//       $product_id = $item_data['product_id'];

       # Targeting a defined product ID; print or bundle
       if ( 69711 == $product_id || $product_id == 137861
        || 69710 == $product_id || $product_id == 137862  // USA print, combo
        ) {
        $print_subscribe = true;
        break;
       }
   }

  if ( !$print_subscribe )
    return;

  global $BYO_pcs_data;

  // pull customer data from order
  $customer_data = array();
  $customer_data[ 'shipping_first_name'] = $order->get_shipping_first_name();
  $customer_data[ 'shipping_last_name'] = $order->get_shipping_last_name();
  $customer_data[ 'shipping_address_1'] = substr( $order->get_shipping_address_1(), 0, 30);
  $customer_data[ 'shipping_address_2'] = substr( $order->get_shipping_address_2(), 0, 30);
  $customer_data[ 'shipping_city'] = substr( $order->get_shipping_city(), 0, 16);
  $customer_data[ 'shipping_state'] = $order->get_shipping_state();
  $customer_data[ 'shipping_postcode'] = $order->get_shipping_postcode();
  $customer_data[ 'shipping_country'] = $order->get_shipping_country();

  // convert country code from WooCommerce to PCS
  $customer_data[ 'shipping_country'] = BYO_convert_country_code( $customer_data[ 'shipping_country']);

  $product_data = array();
  $product_data[ 'ID'] = $product_id;

  if ( $product_id == 69711 || $product_id == 137861) // Print Only
  {
    if ( $customer_data[ 'shipping_country'] == 'USA') {
      // USA
      $product_data[ 'price'] = '29.99';  // print
      $BYO_pcs_data[ 'TRACK'] = '9SW1F'; // new order
    } elseif ( $customer_data[ 'shipping_country'] == 'CAN' || $customer_data[ 'shipping_country'] == 'MEX') {
      // Canada/Mexico
      $product_data[ 'price'] = '34.99';  //
      $BYO_pcs_data[ 'TRACK'] = '9SW1M'; //
      if( $customer_data[ 'shipping_country'] == 'MEX') {
        $customer_data[ 'shipping_state'] = '';
      }
    } else {
      // Intl., rest of the world
      $product_data[ 'price'] = '49.99';  //
      $BYO_pcs_data[ 'TRACK'] = '9SW3M'; //
      $customer_data[ 'shipping_state'] = '';
    }

  }
  else if ( $product_id == 69710 || $product_id ==  137862) { // Print/Digital combo
    if ( $customer_data[ 'shipping_country'] == 'USA') {
      // USA
      $product_data[ 'price'] = '24.48';  // combo
      $BYO_pcs_data[ 'TRACK'] = '9SW3F'; // new bundle

    } elseif ( $customer_data[ 'shipping_country'] == 'CAN' || $customer_data[ 'shipping_country'] == 'MEX') {
      // Canada/Mexico
      $product_data[ 'price'] = '27.49';  //
      $BYO_pcs_data[ 'TRACK'] = '9SW2M'; //
      if( $customer_data[ 'shipping_country'] == 'MEX') {
        $customer_data[ 'shipping_state'] = '';
      }
    } else {
      // Intl., rest of the world
      $product_data[ 'price'] = '42.49';  //
      $BYO_pcs_data[ 'TRACK'] = '9SW4M'; //
      $customer_data[ 'shipping_state'] = '';
    }
  }



  $headers = $body = '';
  BYO_SetData( $headers, $body, $BYO_pcs_data, $customer_data, $product_data);

  $args = array( 'headers' => $headers, 'body' => $body);

  //var_dump( $args);
  //return;

  $response = wp_remote_post( $BYO_pcs_data['URL'], $args);
  //var_dump( $response);

  $string = $response['body'];
  $error = '';
    $result =  BYO_GetSoapResult( $string , $error)  ? 'worked' : 'failed';

  $note = 'The BYO PCS SOAP transaction ' . $result . '. ' . $error;
  $order->add_order_note( $note );

  return( $result);
}

///////////////////////////////////////////////////////////////////////////////

// on renewal action, start transaction with PCS, return results?
//add_action('woocommerce_renewal_complete', 'BYO_TestSoapRenewalOrder', 10, 1);

// TBD

$BYO_pcs_renewal_data = array();
$BYO_pcs_renewal_data[ 'URL'] = 'https://subscribe.pcspublink.com/WSTransactions_v1_1_9/WSTransactions_v1_1_9.asmx';
$BYO_pcs_renewal_data[ 'Host'] = 'subscribe.pcspublink.com';
$BYO_pcs_renewal_data[ 'SOAPActionNewOrder'] = '"https://subscribe.pcspublink.com/WSTransactions_v1_1_9/WSTransactions_v1_1_9.asmx/NewOrderValidate"';

// sandbox test credentials
$BYO_pcs_renewal_data[ 'UserID'] = 'BREW_SWG#%*';
$BYO_pcs_renewal_data[ 'Password'] = '0xF1968C02C753EF19A9606FA5B702C98B';

$BYO_pcs_renewal_data[ 'xmlns'] = 'https://subscribe.pcspublink.com/WSTransactions_v1_1_9/WSTransactions_v1_1_9.asmx';
$BYO_pcs_renewal_data[ 'PUB'] = 'BREW';
$BYO_pcs_renewal_data[ 'TRACK'] = '9SW1F'; // new order

function BYO_TestSoapRenewalOrder( $order_id) {
  if ( !$order_id )
      return;

  // get product info
  // is this a print-subscription?
   $print_subscribe = false;

   // Get order object
   $order = wc_get_order( $order_id );

   // Iterating through each order items (WC_Order_Item_Product objects in WC 3+)
   foreach ( $order->get_items() as $item_id => $item_values ) {

       // Product_id
       $product_id = $item_values->get_product_id();

       // OR the Product id from the item data
       $item_data = $item_values->get_data();
//       $product_id = $item_data['product_id'];

       # Targeting a defined product ID
       if ( $product_id === 18934 ) {
        $print_subscribe = true;
        break;
       }
   }

  $print_subscribe = true;
  if ( !$print_subscribe )
    return;

  $product_data = array();
  $product_data[ 'ID'] = $product_id;
  $product_data[ 'price'] = '29.99';  // temp

  // pull customer data from order
  $customer_data = array();
  $customer_data[ 'shipping_first_name'] = $order->get_shipping_first_name();
  $customer_data[ 'shipping_last_name'] = $order->get_shipping_last_name();
  $customer_data[ 'shipping_address_1'] = $order->get_shipping_address_1();
  $customer_data[ 'shipping_address_2'] = $order->get_shipping_address_2();
  $customer_data[ 'shipping_city'] = $order->get_shipping_city();
  $customer_data[ 'shipping_state'] = $order->get_shipping_state();
  $customer_data[ 'shipping_postcode'] = $order->get_shipping_postcode();
  $customer_data[ 'shipping_country'] = $order->get_shipping_country();

  // fix country code for PCS
  $customer_data[ 'shipping_country'] = ( 'US' == $customer_data[ 'shipping_country'] ) ? 'USA' : $customer_data[ 'shipping_country'];

  $headers = $body = '';
  global $BYO_pcs_data;
  BYO_SetData( $headers, $body, $BYO_pcs_renewal_data, $customer_data, $product_data);

  $args = array( 'headers' => $headers, 'body' => $body);

  //var_dump( $args);
  //return;

  $response = wp_remote_post( $BYO_pcs_data['URL'], $args);
  //var_dump( $response);

  $string = $response['body'];
  $error = '';
    $result =  BYO_GetSoapResult( $string , $error)  ? 'worked' : 'failed';

  $note = 'The BYO PCS SOAP transaction ' . $result . '. ' . $error;
  $order->add_order_note( $note );

  return( $result);
}
